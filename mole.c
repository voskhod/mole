#define _GNU_SOURCE
#include <assert.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

// Help branch predictions.
#define UNLIKELY(expr) __builtin_expect((expr), 0)

void *(*orig_malloc)(size_t) = NULL;
void *(*orig_calloc)(size_t,size_t) = NULL;
void *(*orig_realloc)(void*, size_t) = NULL;
void *(*orig_reallocarray)(void*, size_t, size_t) = NULL;

#define LOG(msg) do { puts("MOLE: "msg); fflush(stdout); } while (0)

// We could load the symbol here, but for a unknown reason the C++ allocator
// calls malloc() before the constructor.
__attribute__((constructor))
void check_sanity()
{
    LOG("check if the malloc's functions are loaded and sane...");

    void *ptr = malloc(1);
    assert(ptr && "malloc() fails for a small allocation: there is fishy");
    LOG("malloc() is ok");

    ptr = calloc(1, 1);
    assert(ptr && "calloc() fails for a small allocation: this is fishy");
    LOG("calloc() is ok");

    ptr = realloc(ptr, 2);
    assert(ptr && "recalloc() fails for a small reallocation: this is fishy");
    LOG("realloc() is ok");

    ptr = reallocarray(ptr, 2, 2);
    assert(ptr && "reallocarray() fails for a small reallocation: this is fishy");
    LOG("reallocarray() is ok");

    free(ptr);

    LOG("Everythings is correctly initalized !");
}

__attribute__((__visibility__("default")))
void *malloc(size_t size)
{
    // Always allocate at least one byte (malloc(0) is not so well defined, it
    // may return NULL or not).
    if (UNLIKELY(size == 0))
        size = 1;

    // Load malloc implementation if it has not been done.
    if (UNLIKELY(orig_malloc == NULL))
        // Weird trick to avoid warning about function pointer cast.
        *(void **) &orig_malloc = dlsym(RTLD_NEXT, "malloc"); 

    void *res = orig_malloc(size);
    if (UNLIKELY(res == NULL)) // Checks if malloc could have failed.
        LOG("malloc() failed: maybe there is no more memory available ?");

    return res;
}

__attribute__((__visibility__("default")))
void *calloc(size_t nmemb, size_t size)
{
    static volatile int is_initializing = 0;

    // Load calloc implementation if it has not been done. This is a bit special
    // as dlsym() relies on calloc. To avoid infinite recursion, if when we
    // initialies orig_calloc, we returns NULL as dlsym() senms to able to cope
    // with a slightly wrong calloc.
    if (UNLIKELY(!orig_calloc))
    {
        if (is_initializing)
            return NULL;
        else
        {
            is_initializing = 1;
            *(void **) &orig_calloc = dlsym(RTLD_NEXT, "calloc"); 
            is_initializing = 0;
        }
      }

    if (UNLIKELY(!size || !nmemb))
    {
        nmemb = 1;
        size = 1;
    }

    void *res = orig_calloc(nmemb, size);
    if (UNLIKELY(!res)) // Checks if calloc could have failed.
        LOG("calloc() failed: maybe there is no more memory available ?");

    return res;
}

__attribute__((__visibility__("default")))
void *realloc(void *ptr, size_t size)
{
    // Load realloc implementation if it has not been done.
    if (UNLIKELY(!orig_realloc))
        *(void **) &orig_realloc = dlsym(RTLD_NEXT, "realloc"); 

    if (UNLIKELY(!size))
        size = 1;

    void *res = orig_realloc(ptr, size);
    if (UNLIKELY(!res))
        LOG("realloc() failed: maybe there is no more memory available ?");

    return res;
}

__attribute__((__visibility__("default")))
void *reallocarray(void *ptr, size_t nmemb, size_t size)
{
    // Load reallocarray implementation if it has not been done.
    if (UNLIKELY(!orig_reallocarray))
        *(void **) &orig_reallocarray = dlsym(RTLD_NEXT, "reallocarray"); 

    if (UNLIKELY(!size || !nmemb))
    {
        nmemb = 1;
        size = 1;
    }

    void *res = orig_reallocarray(ptr, nmemb, size);
    if (UNLIKELY(!res))
        LOG("reallocarray() failed: maybe there is no more memory available ?");

    return res;
}

