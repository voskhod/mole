function check_mole()
{
  test_name="$1"
  will_segv="$2"

  out=$(LD_PRELOAD=./mole.so ./a.out 2>&1)

  if [[ "$out" =~ "SEGV" ]]; then
    has_segv=1
  else
    has_segv=0
  fi

  if [[ ! "$out" =~ "MOLE: Everythings is correctly initalized !" ]]; then
    echo Example "$test_name": MOLE has not able to initalize, abort
    exit 1
  fi

  if [[ has_segv -ne will_segv ]]; then
    if [[ will_segv ]]; then
      echo Example "$test_name" should recived segv but has not, abort.
    else
      echo Example "$test_name" should not recived segv but has, abort.
    fi
    exit 1
  fi

  if [[ will_segv -eq 1 && ! "$out" =~ "failed: maybe there is no more memory available ?" ]]; then
    echo Example "$test_name" has segv but mole has not detected it, abort.
    exit 1
  fi
}

gcc tests/ok_alloc.c
check_mole ok_alloc 0

for fun_to_test in MALLOC CALLOC REALLOC REALLOCARRAY; do
  gcc -D$fun_to_test tests/bad_alloc.c &> /dev/null
  check_mole bad_alloc 1
done

gcc tests/ok_threads.c -pthread
check_mole ok_thread 0

g++ tests/cpp_ok.cc
check_mole cpp_ok 0

echo All test where successfull !
