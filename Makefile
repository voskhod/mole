CFLAGS+=-Wall -g -Wextra -pedantic -std=c11 -fPIC -fvisibility=hidden -fno-builtin -O3 -march=native
LDFLAGS+=-shared -ldl

all: mole.so

mole.so: mole.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o mole.so mole.c

check: mole.so
	bash test.sh

clean:
	rm -f mole.so

.PHONY: all clean check
