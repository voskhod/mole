#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>

void sig_handler(int sig)
{
  puts("SEGV");
  exit(139);
}

int mythread(void *data)
{
  (void)data;

  int *big_array = malloc(4);
  big_array[0] = 8;

  return 0;
}

int main(int argc, char *argv[])
{
  signal(SIGSEGV, sig_handler);

  thrd_t thr[10];
  for(int n = 0; n < 10; ++n)
    thrd_create(&thr[n], mythread, NULL);
  for(int n = 0; n < 10; ++n)
    thrd_join(thr[n], NULL);

  return 0;
}
