#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sig_handler(int sig)
{
  puts("SEGV");
  exit(139);
}

int main()
{
  signal(SIGSEGV, sig_handler);

  int *big_array = NULL;

#if defined(MALLOC)
  big_array = (int *)malloc(-1);
#elif defined(CALLOC)
  big_array = (int *)calloc(-1, 1);
#elif defined(REALLOC)
  big_array = (int *)realloc(malloc(1), -1);
#elif defined(REALLOCARRAY)
  big_array = (int *)reallocarray(malloc(1), -1, 1);
#else
  return 1;
#endif

  big_array[100000] = 8;

  return 0;
}
