#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sig_handler(int sig)
{
  puts("SEGV");
  exit(139);
}

int main()
{
  signal(SIGSEGV, sig_handler);

  int *a = new int[1];
  a[0] = 8;
  delete[] a;
}
