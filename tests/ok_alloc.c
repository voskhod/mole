#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sig_handler(int sig)
{
  puts("SEGV");
  exit(139);
}

int main()
{
  signal(SIGSEGV, sig_handler);

  int *big_array = (int *)malloc(1000*sizeof(int));
  big_array[123] = 8;
  big_array = realloc(big_array, 2000*sizeof(int));
  big_array[1234] = 8;
  free(big_array);

  big_array = calloc(1000, sizeof(int));
  big_array[123] = 8;
  big_array = reallocarray(big_array, 2000, sizeof(int));
  big_array[1234] = 8;
  free(big_array);

  return 0;
}
