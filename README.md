# Malloc Observer of not so Little Errors

MOLE is a dynamic library to helps to determine if a segfault is due to some
malloc issues (probably out of memory) or a programing mistake for compiled
software.

# Build
Run `make` at the root.

# Usage

Let's try to build a small example where we allocate MAX_INT bytes and run it.
```
$ gcc tests/bad_alloc.c -o bad
$ ./bad
> segmentation fault (core dumped)  ./bad
```

Obviously, a segfault occurs but just by looking at the output we don't know
what happened. Now, we rerun our program but with mole loaded.

Warning, do not forget to specify the path of mole the same way you do for
program (otherwise, the linker will search mole in the installed libraries).

```
$ LD_PRELOAD=./mole.so ./bad
> malloc() failed: maybe there is no more memory available ?
> segmentation fault (core dumped)  ./bad
```

Without rebuilding bad, we know that malloc() has failed ! (in this case the
allocation is too big too succeed)
